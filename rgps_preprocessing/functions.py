import os
import shutil
from datetime import date, datetime

from rgps_preprocessing.asf_api import daterange_datapool_download
from rgps_preprocessing.build_geodataframes import daterange_save_dataframes
from rgps_preprocessing.constants import ICE_CONCENTRATION_DIR, DATA_DIR, SENTINEL1, NORTH, SOUTH
from rgps_preprocessing.download_images import clear_cache
from rgps_preprocessing.ice_concentration import daterange_amsr_download_data


def setup_config(working_directory=''):
    """
    Setup the configuration for matching and directories
    TODO: Add more configuration needed for NISAR and other platform compatibilities
 
    :param working_directory: Set working directory
    :return: None
    """
    os.chdir(working_directory)


def setup_hem_daterange(start_date: date, end_date: date, platform: str, hemisphere: str):
    """
    Create and load all data directories for the given date range and save them to the file system
    in yearly, monthly, daily nested directories
 
    :param start_date: starting day (inclusive) of data to save
    :param end_date: ending day (exclusive) of data to save
    :param platform: satellite platform for data
    :param hemisphere: hemisphere of data
    :return: None
    """
    daterange_datapool_download(start_date, end_date, platform, hemisphere)
    daterange_amsr_download_data(start_date, end_date, hemisphere)
    daterange_save_dataframes(start_date, end_date, platform, hemisphere)


def clean():
    """
    Clean all downloaded data
    WARNING: Don't do unless data is corrupted, you will lose hours of work
   
    :return: None
    """
    if DATA_DIR.exists():
        shutil.rmtree(DATA_DIR)
    if ICE_CONCENTRATION_DIR.exists():
        shutil.rmtree(ICE_CONCENTRATION_DIR)
    clear_cache()
