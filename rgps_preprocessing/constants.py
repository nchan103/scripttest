from pathlib import Path

# Paths
WORKDIR = Path('')
RAW_DATA_DIR = WORKDIR / 'raw'
DATA_DIR = WORKDIR / 'preprocessing_data'
DATAPOOL_DIR = WORKDIR / 'datapool'
DATAFRAME_DIR = WORKDIR / 'dataframe'
ICE_CONCENTRATION_DIR = WORKDIR / 'icondata'
AMSR_DIR = DATA_DIR / 'amsr'
CACHE = DATA_DIR / 'cache'


# Hemispheres:
NORTH = 'north'
SOUTH = 'south'

# Platform
SENTINEL1 = 'SENTINEL-1'
NISAR = ''

