from geopandas import GeoDataFrame
import pandas as pd
import json
from shapely.wkt import loads
from datetime import datetime, date
from functools import partial
from rgps_preprocessing.ice_concentration import get_land_and_ice_percentage, build_amsr_path
from pathlib import Path
from rgps_preprocessing.util import daterange, build_daily_path, verify_dir
from rgps_preprocessing.constants import DATAPOOL_DIR, DATA_DIR, DATAFRAME_DIR, NORTH, SENTINEL1
from rgps_preprocessing.geocoding import crs_transform, WGS84_CODE, SSMI_CODES, BBOX_KEY, HEM_KEY, ARCTIC
from rgps_preprocessing.asf_api import datapool_filename

pd.options.display.width = 0
pd.options.display.max_colwidth = 150


def build_daily_gdf(day: date, hemisphere: str, datapool: Path) -> GeoDataFrame:
    """
    Build a GeoDataFrame containing a subset of asf datapool information and ice concentration data
    for a day and hemisphere to use for matching and plotting.
    NOTE: Requires ice concentration and asf datapool data to be downloaded first before running
   
    :param day: date of data to use to construct gdf
    :param hemisphere: hemisphere of data in gdf
    :param datapool: asf datapool file to use to build gdf
    :return: GeoDataFrame containing the day's asf datapool (time and footprint) and ice concentration data
    """
    if not datapool.exists():
        raise FileNotFoundError(f'Datapool {datapool} does not exist. please download datapool data using one of '
                                'rgps_preprocessing.asf_api\'s datapool download functions before proceeding')
    with open(datapool) as f:
        text = f.read()
        df = pd.DataFrame.from_dict(json.loads(text)[0])
    df['stringFootprint'] = df['stringFootprint'].apply(loads)
    df['sceneDate'] = pd.to_datetime(df['sceneDate'], format='%Y-%m-%dT%H:%M:%S.%f')
    gdf = GeoDataFrame(df).set_geometry('stringFootprint')
    gdf = gdf[['productName', 'sceneDate', 'stringFootprint', 'downloadUrl']]
    gdf.crs = WGS84_CODE
    gdf = gdf.to_crs(SSMI_CODES[hemisphere])
    bounds_df = gdf.bounds
    bounds_list = list(bounds_df[['minx', 'miny', 'maxx', 'maxy']].itertuples(index=False, name=None))
    processed_icon_for_bounds = partial(get_land_and_ice_percentage, amsr_path=build_amsr_path(day, hemisphere))
    ice_concentration = map(processed_icon_for_bounds, bounds_list)
    icon_df = pd.DataFrame(ice_concentration,
                           columns=['percent_ice', 'percent_land', 'percent_open_water', 'percent_unknown'])
    gdf = gdf.join(icon_df)
    gdf = gdf.set_index(['sceneDate'])
    return gdf


def load_geodataframe(df_path: Path, hemisphere: str) -> GeoDataFrame:
    """
    Load GeoDataFrame data from a csv and re-convert it into a valid GeoDataFrame for the
    rgps preprocessing system.
   
    :param df_path: Path where the dataframe csv is located
    :param hemisphere: hemisphere of the dataframe's data
    :return: system-valid GeoDataFrame
    """
    df = pd.read_csv(df_path)
    df['stringFootprint'] = df['stringFootprint'].apply(loads)
    df['sceneDate'] = pd.to_datetime(df['sceneDate'], format='%Y-%m-%d %H:%M:%S')
    gdf = GeoDataFrame(df).set_geometry('stringFootprint')
    gdf.crs = SSMI_CODES[hemisphere]
    gdf = gdf.set_index(['sceneDate'])
    return gdf


def daterange_save_dataframes(start_date: date, end_date: date, platform: str, hemisphere: str) -> None:
    """
    Create dataframes for the given date range and save them to the file system
    in yearly, monthly, daily nested directories
   
    :param start_date: starting day (inclusive) of dataframes to save
    :param end_date: ending day (exclusive) of dataframes to save
    :param platform: satellite platform for constructing GeoDataFrames
    :param hemisphere: hemisphere of data in GeoDataFrames
    :return: None
    """
    for day in daterange(start_date, end_date):
        datapool_path = Path(DATA_DIR) / platform / DATAPOOL_DIR
        datapool = build_daily_path(datapool_path, day, hemisphere, file_name=datapool_filename(day))
        dataframe_path = Path(DATA_DIR) / platform / DATAFRAME_DIR
        verify_dir(dataframe_path)
        dataframe_path = build_daily_path(dataframe_path, day, hemisphere, file_name='daily_gdf.csv',
                                          daily_directory=True)
        gdf = build_daily_gdf(day, hemisphere, datapool)
        gdf.to_csv(str(dataframe_path))


def daterange_get_dataframe(start_time: datetime, end_time: datetime, platform: str, bound: dict) -> GeoDataFrame:
    """
    Merge and filter from saved dataframe CSVs to create single dataframe containing all images
    created between start time and end time that are within the bounding box
    NOTE: dataframes must be created with datarange_save_dataframe before being accessed
   
    :param start_time: starting time bound of the returned gdf
    :param end_time: ending time bound of the returned gdf
    :param platform: satellite platform that created the data
    :param bound: bounding box of the returned data (given as constants in rgps_preprocessing.geocoding)
    :return: GeoDataFrame containing data matching the above constraints
    """
    gdf: GeoDataFrame = None
    for day in daterange(start_time.date(), end_time.date()):
        dataframe_path = Path(DATA_DIR) / platform / DATAFRAME_DIR
        dataframe_path = build_daily_path(dataframe_path, day, bound[HEM_KEY], file_name='daily_gdf.csv',
                                          daily_directory=True)
        if not dataframe_path.exists():
            raise FileNotFoundError(f'Dataframe at {dataframe_path} not created yet, '
                                    f'use daterange_save_dataframe to create data first')
        if gdf is None:
            gdf = load_geodataframe(dataframe_path, bound[HEM_KEY])
        else:
            gdf = gdf.append(load_geodataframe(dataframe_path, bound[HEM_KEY]))
    gdf = gdf[(start_time <= gdf.index) & (gdf.index <= end_time)]
    bound_polygon = loads(bound[BBOX_KEY])
    ssmi_bound_polygon = crs_transform(bound_polygon, WGS84_CODE, SSMI_CODES[bound[HEM_KEY]])
    gdf = gdf[gdf.intersection(ssmi_bound_polygon).area > 0]
    return gdf
