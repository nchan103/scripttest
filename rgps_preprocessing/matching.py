import geopandas as gpd
import pandas as pd
from datetime import datetime, timedelta
from pandas.core.series import Series
from rgps_preprocessing.build_geodataframes import daterange_get_dataframe
from rgps_preprocessing.constants import SENTINEL1
from rgps_preprocessing.geocoding import ARCTIC


pd.options.display.width = 0
pd.options.display.max_colwidth = 150
pd.set_option('display.max_rows', None)

# Matching Thresholds
LAND_THRESHOLD = 0.95
OPEN_WATER_THRESHOLD = 0.95
MIN_ICE_THRESHOLD = 0.15
MAX_ICE_THRESHOLD = 1
OVERLAP_THRESHOLD = 0.05
EARLIEST_TIME_THRESHOLD = 2.3
LATEST_TIME_THRESHOLD = 0


def get_target_df_matches(target: Series, gdf: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    """
    Get a GeoDataFrame of all potential source images that match the target image
    Note: RGPS preprocessing was done in such a way that it matches the target image to possible sources

    :param target: target image to match against
    :param gdf: GeoDataFrame of interest containing possible source images (GeoDataFrame from around the same time and in the same region)
    :return: GeoDataFrame of potential source images that meet the thresholds of matching for target image
    """
    if target.percent_land > LAND_THRESHOLD \
            or target.percent_open_water > OPEN_WATER_THRESHOLD:
        return gpd.GeoDataFrame(columns=gdf.columns)
    img_time = target.name
    early_threshold = (img_time - timedelta(days=EARLIEST_TIME_THRESHOLD))
    late_threshold = (img_time + timedelta(days=LATEST_TIME_THRESHOLD))
    return gdf[((gdf.intersection(target.stringFootprint).area / target.stringFootprint.area) > OVERLAP_THRESHOLD) &
               (gdf.percent_land < LAND_THRESHOLD) &
               (gdf.percent_open_water < OPEN_WATER_THRESHOLD) &
               (gdf.percent_ice > MIN_ICE_THRESHOLD) &
               (gdf.percent_ice < MAX_ICE_THRESHOLD) &
               (gdf.index > early_threshold) &
               (gdf.index < late_threshold) &
               (gdf.index != target.name)]


def get_all_matches(start_time: datetime, end_time: datetime, platform: str, bounding_box: dict):
    """
    Get all possible matches in a given bounding box for a given time range
    
    :param start_time: beginning of time range
    :param end_time: end of time range
    :param platform: satellite platform of the matching data
    :param bounding_box: bounding box of interest for matching
    :return:
    """
    gdf = daterange_get_dataframe(start_time, end_time, platform, bounding_box)
    for row in gdf.itertuples():
        matches = get_target_df_matches(row, gdf)
        yield row, matches
