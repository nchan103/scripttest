import subprocess
from datetime import datetime, date, time
from rgps_preprocessing.util import daterange, build_daily_path, verify_dir
from pathlib import Path
from rgps_preprocessing.constants import DATAPOOL_DIR, DATA_DIR, NORTH, SOUTH, SENTINEL1
from rgps_preprocessing.geocoding import BBOX_KEY, ARCTIC, ANTARCTIC


def urlformat(dictionary: dict) -> str:
    """
    Format a dictionary of parameters into url parameter fields.
    I'm not sure why url formatters don't work with the ASF API but this does.
   
    :param dictionary: dictionary of parameters
    :return: url param string
    """
    kwlist = []
    for key, value in dictionary.items():
        kwlist.append(key + "=" + value)
    return '&'.join(kwlist)


def download_asf_data(fields: dict, destination_path: Path, destination_filename: str = None) -> None:
    """
    Download data from asf with the given parameters from asf API using aria2 to destination path
   
   
    :param fields: dict of API parameters as given in https://asf.alaska.edu/api/#keywords
    :param destination_path: destination path for downloads
    :param destination_filename: filename for the downloaded file, original filename if left blank
    :return: None
    """
    url = "https://api.daac.asf.alaska.edu/services/search/param?" + urlformat(fields)
    subprocess.run(" ".join(['aria2c',
                             '--check-certificate=false',
                             '--http-auth-challenge=true',
                             '--http-user=ziyiwu',
                             '--http-passwd="Password1"',
                             '--allow-overwrite="true"',
                             f'-o {destination_filename}' if destination_filename else '',
                             '"' + url + '"',
                             ]), cwd=destination_path, shell=True)


def download_asf_zip(url, destination_path) -> None:
    """
    Download zip file from asf using aria2 to destination path
   
    :param url: url of the zip file
    :param destination_path: destination path of the zipfile
    :return: None
    """
    subprocess.run(" ".join(['aria2c',
                             '--check-certificate=false',
                             '--http-auth-challenge=true',
                             '--http-user=ziyiwu',
                             '--http-passwd="Password1"',
                             '"' + url + '"'
                             ]), cwd=destination_path, shell=True)


def daily_datapool_download(day: date, platform: str, hemisphere: str) -> None:
    """
    Update files from ASF API based on the last download date to the datapool
    directory of the given platform
  
    :param day: day for download asf datapool
    :param platform: Satellite platform
    :param hemisphere: Hemisphere of to download
    :return: None
    """

    platform_dir = Path(DATA_DIR)/platform
    datapool_dir = platform_dir/DATAPOOL_DIR
    verify_dir(datapool_dir)
    day_start = datetime.combine(day, time.min).strftime("%Y-%m-%dT%H:%M:%S")
    day_end = datetime.combine(day, time.max).strftime("%Y-%m-%dT%H:%M:%S")
    bbox = None
    if hemisphere == NORTH:
        bbox = ARCTIC[BBOX_KEY]
    elif hemisphere == SOUTH:
        bbox = ANTARCTIC[BBOX_KEY]
    fields = {'platform': platform,
              'instrument': 'C-SAR',
              'intersectsWith': bbox,
              'start': day_start + 'Z',
              'end': day_end + 'Z',
              'output': 'JSON',
              'processingLevel': 'GRD_HS,GRD_HD,GRD_MD,GRD_MS',
              'beamSwath': 'EW',
              }
    datapool_path = build_daily_path(datapool_dir, day, hemisphere)
    download_asf_data(fields, datapool_path, destination_filename=datapool_filename(day))


def daterange_datapool_download(start_date: date, end_date: date, platform, hemisphere) -> None:
    """
    Download asf datapool for days between start_date (inclusive) and end_date (exclusive)
  
    :param start_date: state day for downloads
    :param end_date: end day for downloads
    :param platform: satellite platform for download
    :param hemisphere: hemisphere to download
    :return: None
    """
    for single_date in daterange(start_date, end_date):
        daily_datapool_download(single_date, platform, hemisphere)


def datapool_filename(day: date) -> str:
    """
    Generate a filename for the datapool containing the day of the data
    to replace the default day of download
  
    :param day: day of data acquisition
    :return: datapool filename
    """
    date_string = day.strftime("%Y-%m-%d")
    datapool_name = f'asf-datapool-results-{date_string}.jsonl'
    return datapool_name
