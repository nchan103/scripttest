from datetime import date, datetime
from typing import Tuple
from pathlib import Path
import os
import shutil
import warnings

from zipfile import ZipFile, BadZipFile
import geopandas as gpd
import json
from pandas.core.series import Series
from rgps_preprocessing.asf_api import download_asf_zip
from rgps_preprocessing.util import verify_dir
from rgps_preprocessing.matching import daterange_get_dataframe, get_target_df_matches
from rgps_preprocessing.geocoding import ARCTIC
from rgps_preprocessing.constants import SENTINEL1, CACHE

cache = set()


def download_all(target: Series, sources: gpd.GeoDataFrame) -> Tuple[Series, gpd.GeoDataFrame]:
    """
    Given a target source pair, download all the images, using the cache if possible
   
    :param target: Target image metadata GeoSeries
    :param sources: GeoDataFrame containing sources images
    :return: Tuple of target and sources with the paths of each image in the src_path attribute
    """
    load_cache()
    src_paths = []
    verify_dir(CACHE)
    target.src_path = download_if_not_cached(target)
    for row in sources.itertuples():
        src_paths.append(download_if_not_cached(row))
    sources['src_path'] = src_paths
    save_cache()
    return target, sources


def save_cache():
    """
    Write the contents of cache to a file
   
    :return: None
    """
    global cache
    if not os.path.exists(CACHE):
        os.makedirs(CACHE)
    cache_file = os.path.join(CACHE, 'cache.json')
    with open(cache_file, 'w') as f:
        f.write(json.dumps(list(cache)))


def load_cache():
    """
    Read the contents of the saved cache file to cache
 
    :return:
    """
    global cache
    cache_file = os.path.join(CACHE, 'cache.json')
    if os.path.exists(cache_file):
        try:
            with open(cache_file) as f:
                cache = set(json.loads(f.read()))
        except Exception as e:
            print(f'Could not open cache: {e}')


def download_if_not_cached(product: Series) -> Path:
    """
    Download images if they are not in the cache
  
    :param product: An image metadata GeoSeries
    :return: Path to the downloaded image
    """
    global cache
    if product.productName not in cache:
        src_path = extract_geotiff(CACHE, product.downloadUrl, product.productName)
        cache.add(product.productName)
        return src_path

# TODO: Determine how to run RGPS
# def run_matches(target: Series, sources: gpd.GeoDataFrame):
#     """
#
#     :param target:
#     :param sources:
#     :return:
#     """
#     target, sources = download_all(target, sources)
#     for row in sources.itertuples():
#         run_rgps(target.src_path, row.src_path)
#
#
# def run_rgps(src_path: Path, tgt_path: Path):
#     pass


def extract_zip(path: Path):
    """
    Extract a zipfile at path into it's parent directory
   
    :param path: Path to the zipfile
    :return:
    """
    try:
        with ZipFile(path) as zipObj:
            zipObj.extractall(path.parent)
    except BadZipFile:
        print(str(path), " is not a zipfile.")


def extract_geotiff(dir_path: Path, download_url: str, product_name: str):
    """
    Extract the hh geotiff from for a specified product name and download url
  
    :param dir_path: Directory to download images into
    :param download_url: Url of download
    :param product_name: Name of the data product
    :return: Path of the extracted geotiff
    """
    download_asf_zip(download_url, dir_path)
    zipfile_path = (dir_path / product_name).with_suffix('.zip')
    extract_zip(zipfile_path)
    # SAFE stands for Standard Archive Format for Europe
    # https://asf.alaska.edu/information/data-formats/data-formats-in-depth/
    safe_dir = (dir_path / product_name).with_suffix('.SAFE')
    tiff_source_dir = safe_dir / 'measurement'
    dest_file_path = None
    for filename in os.listdir(tiff_source_dir):
        polarization = filename.split('-')[3]
        if polarization == 'hh':
            dest_file_path = dir_path / filename
            if dest_file_path.exists():
                warnings.warn(f'Attempting to write image {dest_file_path} already exists')
                return
            shutil.move((tiff_source_dir / filename), dir_path)
    shutil.rmtree(safe_dir)
    os.remove(zipfile_path)
    geocode_image(dest_file_path)
    return dest_file_path


def geocode_image(img_path: Path):
    """
    TODO: Use gdal to geocode image at path
    this function has not been implemented yet
    :return:
    """
    pass


def clear_cache():
    """
    Clear all cached images
   
    :return: None
    """
    if CACHE.exists():
        shutil.rmtree(CACHE)



