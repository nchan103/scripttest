import os

import pyproj
from osgeo import gdal
from shapely.geometry import polygon
from shapely.ops import transform
from rgps_preprocessing.constants import NORTH, SOUTH
from pyproj import Transformer
from typing import Tuple
from pathlib import Path

# EPSG Codes
WGS84_CODE = 'EPSG:4326'
SSMI_CODES = {
    NORTH: 'EPSG:3413',
    SOUTH: 'EPSG:3976'
}

# Bounding boxes as dictionaries
BBOX_KEY = 'bounding_box'
HEM_KEY = 'hem'
NAME_KEY = 'name'
ARCTIC = {NAME_KEY: 'Arctic', BBOX_KEY: "POLYGON((-100 65,5 70,100 70,-175 65,-100 65))", HEM_KEY: NORTH}
ANTARCTIC = {NAME_KEY: 'Antarctic', BBOX_KEY: "POLYGON((-135 -50,128 -45,45 -40,-35 -45,-135 -50))", HEM_KEY: SOUTH}
BEAUFORT = {NAME_KEY: 'Beaufort', BBOX_KEY: "POLYGON((-179 70, -179 88,-70 88, -130 70, -179 70))", HEM_KEY: NORTH}
WEDDELL = {NAME_KEY: 'Weddell', BBOX_KEY: "POLYGON((-60 -78, -60 -54, -15 -54, -15 -78))", HEM_KEY: NORTH}


def warp_dataset(amsr_path: Path, warp_options: dict):
    """
    Call gdal.Warp with specified options on input AMSR file.
    NOTE: not sure if this belongs here, I'll leave it up to your discretion

    :param amsr_path: Path to daily AMSR file
    :param warp_options: A dictionary containing options for `gdal.Warp`
    :returns: A 2d `numpy.ndarray` with ice concentration values
    """
    # Open GeoTIFF as GDAL Dataset (gds)
    gds = gdal.Open(str(amsr_path))
    # Temp tiff file
    tmp_tiff = 'tmp.tiff'
    icecon_warp = gdal.Warp(tmp_tiff, gds, **warp_options)
    # Get numeric values
    icecon_data = icecon_warp.ReadAsArray()
    # Close dummy ds
    del icecon_warp
    # Delete temporary tiff
    os.remove(tmp_tiff)
    # Return the numeric ice concentration values
    return icecon_data


def lonlat2ssmi(lat, lon, hem):
    """
    Convert longitude and latitude (WGS84) to SSMI coordinates
    NOTE: not sure if this belongs here either

    :param lat: latitude (WGS84)
    :param lon: longitude (WGS84)
    :param hem: hemisphere- NORTH or 'south'
    :returns: x and y coordinates in SSMI projection
    """
    # Prepare pyproj Transformer
    transformer = Transformer.from_crs(WGS84_CODE, SSMI_CODES[hem])
    # Convert lon lat to SSMI
    xm, ym = transformer.transform(lat, lon)
    return xm, ym


def get_hemisphere(bounding_box: Tuple[int, int, int, int]):
    """
    TODO: find a better way to figure out hemispheres
    Get the hemisphere of the given bounding box

    :param bounding_box: tuple of the form (ul_lat, ul_lon, lr_lat, lr_lon)
    :return: hemisphere containing the bounding box
    """
    return NORTH if bounding_box[2] > 0 else SOUTH


def crs_transform(src_polygon: polygon, src_crs: str, tgt_crs: str):
    """
    Transforms a Well Known Text shape from source coordinate reference system to target coordinate reference system
    implementation of https://gis.stackexchange.com/questions/127427/transforming-shapely-polygon-and-multipolygon-objects
  
    :param src_polygon: source shapely polygon
    :param src_crs: Source coordinate reference system
    :param tgt_crs: Target coordinate reference system
    :return: well known text string transformed to target coordinate system
    """
    project = pyproj.Transformer.from_proj(
        pyproj.Proj(src_crs),  # source coordinate system
        pyproj.Proj(tgt_crs), always_xy=True)  # target coordinate system
    return transform(project.transform, src_polygon)

class GdalRaster(object):
    '''
    GDAL Raster class. Opens GeoTIFF using GDAL, geocodes, and extracts useful
    metadata. Includes methods for:

    '''
    def __init__(self, filepath, **kwargs):
        '''
        Initialization
     
        :param filepath: Path to GeoTIFF
        :param hem: Hemisphere string ('north' or 'south')
        :param pixsp: Pixel spacing (in meters)
        '''
        # Extract kwargs
        pixsp = kwargs.get('pixsp', 100) # Default 100m
        hem = kwargs.get('hem', 'north') #Default northern hemisphere
        reproj = kwargs.get('reproj', True) #Default is to reproject
        # Validate filepath
        self.filepath = filepath
        if not(os.path.exists(os.path.abspath(filepath))):
            # sys.exit('%s does not exist.' % filepath)
            print('Warning: %s does not exist.' % filepath)
        self.dir, self.name = os.path.split(os.path.abspath(filepath))
        # Validate hemisphere
        if hem not in VALID_HEMISPHERES:
            sys.exit('Invalid hemisphere: %s' % hem)
        self.hem = hem
        # Open GeoTIFF
        self.raster = gdal.Open(self.filepath)
        # Reproject
        if reproj:
            self.reproject(pixsp)
        ## Collect metadata
        # Date
        datetime_obj = datetime.strptime(
            self.raster.GetMetadata()['TIFFTAG_DATETIME'],
            '%Y:%m:%d %H:%M:%S'
        )
        self.year = datetime_obj.year
        self.doy = int(datetime.strftime(datetime_obj, '%j')) \
            + int(datetime_obj.hour)/24 + int(datetime_obj.minute)/(24*60) \
            + int(datetime_obj.second)/(24*60*60)

    
    def reproject(self, pixsp):
        '''
        Reproject input GeoTIFF in memory from WSG86 to SSMI
        '''
        self.reprojected = '/vsimem/'+self.name.replace('.tiff','.reproj.tiff')
        gdal.Warp(
            self.reprojected, self.raster, 
            srcSRS='EPSG:4326', dstSRS=SSMI_CODES[self.hem],
            xRes=pixsp, yRes=pixsp
        )

    def get_extent(self):
        xmin, xpixel, _, ymax, _, ypixel = self.raster.GetGeoTransform()
        width, height = self.raster.RasterXSize, self.raster.RasterYSize
        xmax = xmin + width * xpixel
        ymin = ymax + height * ypixel
        return (xmin, ymax), (xmax, ymax), (xmax, ymin), (xmin, ymin)   

    def raw_to_array(self):
        raw = self.read_to_raw()
        data = raw.read(1, out_shape=(1, int(raw.height), int(raw.width)))
        return(data)

    def close(self):
        self.raw.close()