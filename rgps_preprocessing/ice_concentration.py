import os
import numpy as np
from rgps_preprocessing.geocoding import warp_dataset
from datetime import date
import requests
from pathlib import Path
from rgps_preprocessing.util import daterange, build_daily_path, verify_dir
from rgps_preprocessing.constants import NORTH, SOUTH, ICE_CONCENTRATION_DIR, AMSR_DIR
from rgps_preprocessing.geocoding import NAME_KEY, ARCTIC, ANTARCTIC
from tqdm import tqdm


# ASMR Ice Concentration Values
# 0: Open water
OPEN_WATER = 0
# 1-100: Sea ice (%)
ICE_LOWER_LIMIT = 30
ICE_UPPER_LIMIT = 100
# 120: Land
LAND = 120
# 101-119 & >120: Unknown


def get_land_and_ice_percentage(bounding_box, amsr_path: Path = Path("")):
    """
    Extract land and ice coverage as percent of total image area
    NOTE: I'm assuming we're only using AMSR data for now
    Also, I'm assuming we can get both land and ice out of this, though
    it might turn out that we can only use AMSR for ice due to resolution

    :param bounding_box: bounding box of the form (ul_xmeters, ul_ymeters, lr_xmeters, lr_ymeters)
    :param amsr_path: Path to AMSR file (should be uniquely identified by datestr)
    :returns: land and ice coverage as percents
    """
    # Calculate lower right corner projected coordinates (meters)
    # Prepare gdal.Warp options
    outputBounds = bounding_box # (ulxm, lrym, lrxm, ulym)
    warp_options = {
        'format': 'GTiff',
        'outputBounds': outputBounds,  # (minX, minY, maxX, maxY) in target SRS
    }
    # Call gdal.Warp with options
    icecon_data = warp_dataset(amsr_path, warp_options)

    ## Calculate surface class coverage
    # NOTE: hard-coded values below are specific to AMSR data
    # Total number of grid cells
    num_cells = icecon_data.size
    openw_mask = icecon_data == OPEN_WATER
    # percent openw
    percent_open_water = np.sum(openw_mask) / num_cells
    # Sea ice mask
    ice_mask = (icecon_data >= ICE_LOWER_LIMIT) & (icecon_data <= ICE_UPPER_LIMIT)
    # Percent sea ice
    percent_ice = np.sum(ice_mask) / num_cells
    # Land mask
    land_mask = icecon_data == LAND
    # Percent land
    percent_land = np.sum(land_mask) / num_cells
    # Unknown mask
    unk_mask = (land_mask + ice_mask + openw_mask) == 0  # 0: not in any of the other classes
    # Percent unknown
    percent_unknown = np.sum(unk_mask) / num_cells

    return percent_ice, percent_land, percent_open_water, percent_unknown  # do we need all these?


def build_amsr_filename(day: date, hemisphere: str) -> str:
    """
    Build file name for AMSR ice concentration files for local filesystem
    and http request

    :param day: date of the ice concentration file
    :param hemisphere: hemisphere of the ice concentration file
    :return: AMSR ice concentration filename
    """
    amsr_res = 'n6250' if hemisphere == NORTH else 's6250'
    amsr_fmt_time = day.strftime("%Y%m%d")
    amsr_file_name = f"asi-AMSR2-{amsr_res}-{amsr_fmt_time}-v5.4.tif"
    return amsr_file_name


def build_amsr_path(day: date, hemisphere: str) -> Path:
    """
    Get path of AMSR ice concentration file for the given day and hemisphere,
    creating it if it does not exist

    :param day: date of the ice concentration file
    :param hemisphere: hemisphere of the ice concentration file
    :return: ice concentration file path
    """
    icon_path = ICE_CONCENTRATION_DIR / AMSR_DIR
    verify_dir(icon_path)
    return build_daily_path(icon_path, day, hemisphere, file_name=build_amsr_filename(day, hemisphere))


def build_amsr_url(day: date, hemisphere: str) -> str:
    """
    Get url for downloading AMSR ice concentration data from the University of Bremen's file system

    :param day: date of ice concentration file
    :param hemisphere: hemisphere of ice concentration file
    :return: URL of ice concentration file
    """
    amsr_url = "https://seaice.uni-bremen.de/data/amsr2/asi_daygrid_swath/"
    amsr_res = 'n6250' if hemisphere == NORTH else 's6250'
    amsr_fmt_time = day.strftime("%Y%m%d")
    amsr_file_name = f"asi-AMSR2-{amsr_res}-{amsr_fmt_time}-v5.4.tif"
    month_abbr = day.strftime("%b").lower()
    amsr_region = ARCTIC[NAME_KEY] if hemisphere == NORTH else ANTARCTIC[NAME_KEY]
    amsr_file_url = f'{amsr_url}/{amsr_res}/{str(day.year)}/{month_abbr}/{amsr_region}/{amsr_file_name}'
    return amsr_file_url


def daily_download_amsr_data(day: date, hemisphere: str) -> None:
    """
    Download a single day's AMSR ice concentration data from the University of Bremen's file system

    :param day: date of ice concentration data to be downloaded
    :param hemisphere: hemisphere of ice concentration data to be downloaded
    :return: None
    :raises IOError: if file cannot be downloaded (the University's website has been down a couple times so far)
    """
    amsr_file_url = build_amsr_url(day, hemisphere)
    amsr_destination_path = build_amsr_path(day, hemisphere)
    download_error = 0
    amsr_img = None
    while download_error < 3:
        amsr_img = requests.get(amsr_file_url)
        if amsr_img.status_code == 200:
            break
        else:
            download_error += 1
    if download_error == 3:
        raise IOError(f'An ice concentration file could not download: {amsr_file_url}')
    with open(amsr_destination_path, 'wb+') as af:
        af.write(amsr_img.content)


def daterange_amsr_download_data(start_date: date, end_date: date, hemisphere: str) -> None:
    """
    Download AMSR ice concentration data for the given date range (inclusive, exclusive) from the
    University of Bremen's file system
    
    :param start_date: start date (inclusive) of ice concentration data to be downloaded
    :param end_date: end date (exclusive) of ice concentration data to be downloaded
    :param hemisphere: hemisphere of ice concentration data to be downloaded
    :return: None
    :raises IOError: if file cannot be downloaded (the University's website has been down a couple times so far)
    """
    for single_date in tqdm(daterange(start_date, end_date)):
        daily_download_amsr_data(single_date, hemisphere)
