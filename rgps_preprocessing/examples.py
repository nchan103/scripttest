from datetime import datetime, date

from rgps_preprocessing.asf_api import daterange_datapool_download
from rgps_preprocessing.build_geodataframes import daterange_get_dataframe, daterange_save_dataframes
from rgps_preprocessing.constants import SENTINEL1, NORTH, SOUTH
from rgps_preprocessing.download_images import download_all
from rgps_preprocessing.functions import setup_hem_daterange
from rgps_preprocessing.geocoding import ARCTIC
from rgps_preprocessing.matching import get_target_df_matches, get_all_matches


def example_download_images():
    """
    Generates a simple subset of images to test downloading, setup first
  
    :return: None
    """
    d1 = datetime(2019, 1, 1)
    d2 = datetime(2019, 1, 31)
    gdf = daterange_get_dataframe(d1, d2, SENTINEL1, ARCTIC)
    gdf = gdf[~gdf.index.duplicated(keep='first')]
    target = gdf.iloc[0]
    sources = get_target_df_matches(target, gdf)
    target, sources = download_all(target, sources)
    print(sources)


def example_datapool_download():
    s_date = date(2019, 1, 1)
    e_date = date(2019, 12, 31)
    daterange_datapool_download(s_date, e_date, SENTINEL1, NORTH)
    daterange_datapool_download(s_date, e_date, SENTINEL1, SOUTH)


def example_get_all_matches():
    """
    Example for get all matches for a short time period
   
    :return: None
    """
    d1 = datetime(2019, 1, 1)
    d2 = datetime(2019, 1, 2)
    get_all_matches(d1, d2, SENTINEL1, ARCTIC)


def example_setup():
    """
    Runs setup over January 2019
   
    :return: None
    """
    d1 = datetime(2019, 1, 1)
    d2 = datetime(2019, 12, 31)
    setup_hem_daterange(d1, d2, SENTINEL1, NORTH)
    setup_hem_daterange(d1, d2, SENTINEL1, SOUTH)


def example_save_dataframes():
    s_date = date(2019, 1, 1)
    e_date = date(2019, 1, 2)
    daterange_save_dataframes(s_date, e_date, SENTINEL1, NORTH)


def example_get_dataframes():
    d1 = datetime(2019, 1, 1)
    d2 = datetime(2019, 1, 2)
    daterange_get_dataframe(d1, d2, SENTINEL1, ARCTIC)
