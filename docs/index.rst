.. RGPS-Preprocessing documentation master file, created by
   sphinx-quickstart on Wed Aug  3 21:59:21 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RGPS-Preprocessing's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
