# ASF Data Preprocessing

## Installation

install [GDAL](https://gdal.org/download.html) v. 3.2.1

install [aria2](https://aria2.github.io/) and add it to PATH

Clone this repo into your project and run:
```
pip install rgps_preprocessing
```

## Installation (as of 4/5/2023 on an M1 MacBook)
There were a number of compatibility issues when trying to use the old installation instructions.

### System Dependencies:
- MacOS 13.2.1
- Python 3.11.2
- GDAL 3.6.3
- aria2c 1.36.0

#### Homebrew installation
```
brew install python3
brew install gdal
brew install aria2
```
#### Setup python3 virtualenv:
```
python3 -m virtualenv prenv
source prenv/bin/activate
pip3 install -r requirements.txt
```

Using an updated requirements.txt (the only packages with forced version numbers are pandas and geopandas in order to ensure backwards compatability with the DataFrame.append() method used in our code):
```
beautifulsoup4
Cartopy
descartes
Fiona
GDAL
pandas==1.2.1
geopandas==0.8.2
imageio
matplotlib
munch
networkx
Pillow
prettytable
pyproj
pyshp
python-dateutil
pytz
requests
scikit-image
scikit-learn
scipy
tqdm
```


Then install the `rgps_preprocessing` package by navigating to the directory that contains the `setup.py` file and running:
`(prenv) python3 setup.py install`


Looking at the list of installed packages afterward:
```
$ pip3 freeze
attrs==22.2.0
beautifulsoup4==4.12.1
Cartopy==0.21.1
certifi==2022.12.7
charset-normalizer==3.1.0
click==8.1.3
click-plugins==1.1.1
cligj==0.7.2
contourpy==1.0.7
cycler==0.11.0
descartes==1.1.0
Fiona==1.9.2
fonttools==4.39.3
GDAL==3.6.3
geopandas==0.8.2
idna==3.4
imageio==2.27.0
joblib==1.2.0
kiwisolver==1.4.4
lazy_loader==0.2
matplotlib==3.7.1
munch==2.5.0
networkx==3.1
numpy==1.24.2
packaging==23.0
pandas==1.2.1
Pillow==9.5.0
prettytable==3.6.0
pyparsing==3.0.9
pyproj==3.5.0
pyshp==2.3.1
python-dateutil==2.8.2
pytz==2023.3
PyWavelets==1.4.1
requests==2.28.2
rgps-preprocessing==0.0.1
scikit-image==0.20.0
scikit-learn==1.2.2
scipy==1.10.1
shapely==2.0.1
six==1.16.0
soupsieve==2.4
threadpoolctl==3.1.0
tifffile==2023.3.21
tqdm==4.65.0
urllib3==1.26.15
wcwidth==0.2.6
```


## TODOs

- Write Tests
- Add Geocoding for geotiffs 
- Add a correct list of dependencies to install in setup.cfg/setup.py
- Make and setup configurations for NISAR

Other TODOs are written in comments

## Usage

This library has functions to set up directories 
containing satellite and ice concentration data 
and use them to generate matches. Some modules
have example functions in `example.py` to show how
to use functions. The main functionality of this module
is shown in the example `example.example_setup` and `example.example_download_images`.

## Example usage
nisenv
python3
import sys
sys.path.append('rgps_preprocessing')
from examples import *

# Need to run example_setup() first (setup_hem_daterange):
example_setup()

# Try to run example_download_images():
 FileNotFoundError: Dataframe at preprocessing_data/SENTINEL-1/dataframe/north/2019/jan/30/daily_gdf.csv not created yet, use daterange_save_dataframe to create data first

 # Try to use daterange_save_dataframe




 # you can see the rgps_preprocessing library's documentation in rgps-preprocessing.pdf
