# rgps_preprocessing package

## Submodules

## rgps_preprocessing.asf_api module


### rgps_preprocessing.asf_api.daily_datapool_download(day: datetime.date, platform: str, hemisphere: str)
Update files from ASF API based on the last download date to the datapool
directory of the given platform


* **Parameters**

    
    * **day** – day for download asf datapool


    * **platform** – Satellite platform


    * **hemisphere** – Hemisphere of to download



* **Returns**

    None



### rgps_preprocessing.asf_api.datapool_filename(day: datetime.date)
Generate a filename for the datapool containing the day of the data
to replace the default day of download


* **Parameters**

    **day** – day of data acquisition



* **Returns**

    datapool filename



### rgps_preprocessing.asf_api.daterange_datapool_download(start_date: datetime.date, end_date: datetime.date, platform, hemisphere)
Download asf datapool for days between start_date (inclusive) and end_date (exclusive)


* **Parameters**

    
    * **start_date** – state day for downloads


    * **end_date** – end day for downloads


    * **platform** – satellite platform for download


    * **hemisphere** – hemisphere to download



* **Returns**

    None



### rgps_preprocessing.asf_api.download_asf_data(fields: dict, destination_path: pathlib.Path, destination_filename: Optional[str] = None)
Download data from asf with the given parameters from asf API using aria2 to destination path


* **Parameters**

    
    * **fields** – dict of API parameters as given in [https://asf.alaska.edu/api/#keywords](https://asf.alaska.edu/api/#keywords)


    * **destination_path** – destination path for downloads


    * **destination_filename** – filename for the downloaded file, original filename if left blank



* **Returns**

    None



### rgps_preprocessing.asf_api.download_asf_zip(url, destination_path)
Download zip file from asf using aria2 to destination path


* **Parameters**

    
    * **url** – url of the zip file


    * **destination_path** – destination path of the zipfile



* **Returns**

    None



### rgps_preprocessing.asf_api.urlformat(dictionary: dict)
Format a dictionary of parameters into url parameter fields.
I’m not sure why url formatters don’t work with the ASF API but this does.


* **Parameters**

    **dictionary** – dictionary of parameters



* **Returns**

    url param string


## rgps_preprocessing.build_geodataframes module


### rgps_preprocessing.build_geodataframes.build_daily_gdf(day: datetime.date, hemisphere: str, datapool: pathlib.Path)
Build a GeoDataFrame containing a subset of asf datapool information and ice concentration data
for a day and hemisphere to use for matching and plotting.
NOTE: Requires ice concentration and asf datapool data to be downloaded first before running


* **Parameters**

    
    * **day** – date of data to use to construct gdf


    * **hemisphere** – hemisphere of data in gdf


    * **datapool** – asf datapool file to use to build gdf



* **Returns**

    GeoDataFrame containing the day’s asf datapool (time and footprint) and ice concentration data



### rgps_preprocessing.build_geodataframes.daterange_get_dataframe(start_time: datetime.datetime, end_time: datetime.datetime, platform: str, bound: dict)
Merge and filter from saved dataframe CSVs to create single dataframe containing all images
created between start time and end time that are within the bounding box
NOTE: dataframes must be created with datarange_save_dataframe before being accessed


* **Parameters**

    
    * **start_time** – starting time bound of the returned gdf


    * **end_time** – ending time bound of the returned gdf


    * **platform** – satellite platform that created the data


    * **bound** – bounding box of the returned data (given as constants in rgps_preprocessing.geocoding)



* **Returns**

    GeoDataFrame containing data matching the above constraints



### rgps_preprocessing.build_geodataframes.daterange_save_dataframes(start_date: datetime.date, end_date: datetime.date, platform: str, hemisphere: str)
Create dataframes for the given date range and save them to the file system
in yearly, monthly, daily nested directories


* **Parameters**

    
    * **start_date** – starting day (inclusive) of dataframes to save


    * **end_date** – ending day (exclusive) of dataframes to save


    * **platform** – satellite platform for constructing GeoDataFrames


    * **hemisphere** – hemisphere of data in GeoDataFrames



* **Returns**

    None



### rgps_preprocessing.build_geodataframes.load_geodataframe(df_path: pathlib.Path, hemisphere: str)
Load GeoDataFrame data from a csv and re-convert it into a valid GeoDataFrame for the
rgps preprocessing system.


* **Parameters**

    
    * **df_path** – Path where the dataframe csv is located


    * **hemisphere** – hemisphere of the dataframe’s data



* **Returns**

    system-valid GeoDataFrame


## rgps_preprocessing.constants module

## rgps_preprocessing.download_images module


### rgps_preprocessing.download_images.clear_cache()
Clear all cached images


* **Returns**

    None



### rgps_preprocessing.download_images.download_all(target: pandas.core.series.Series, sources: geopandas.geodataframe.GeoDataFrame)
Given a target source pair, download all the images, using the cache if possible


* **Parameters**

    
    * **target** – Target image metadata GeoSeries


    * **sources** – GeoDataFrame containing sources images



* **Returns**

    Tuple of target and sources with the paths of each image in the src_path attribute



### rgps_preprocessing.download_images.download_if_not_cached(product: pandas.core.series.Series)
Download images if they are not in the cache


* **Parameters**

    **product** – An image metadata GeoSeries



* **Returns**

    Path to the downloaded image



### rgps_preprocessing.download_images.extract_geotiff(dir_path: pathlib.Path, download_url: str, product_name: str)
Extract the hh geotiff from for a specified product name and download url


* **Parameters**

    
    * **dir_path** – Directory to download images into


    * **download_url** – Url of download


    * **product_name** – Name of the data product



* **Returns**

    Path of the extracted geotiff



### rgps_preprocessing.download_images.extract_zip(path: pathlib.Path)
Extract a zipfile at path into it’s parent directory


* **Parameters**

    **path** – Path to the zipfile



* **Returns**

    


### rgps_preprocessing.download_images.geocode_image(img_path: pathlib.Path)
TODO: Use gdal to geocode image at path
this function has not been implemented yet
:return:


### rgps_preprocessing.download_images.load_cache()
Read the contents of the saved cache file to cache


* **Returns**

    


### rgps_preprocessing.download_images.save_cache()
Write the contents of cache to a file


* **Returns**

    None


## rgps_preprocessing.examples module


### rgps_preprocessing.examples.example_datapool_download()

### rgps_preprocessing.examples.example_download_images()
Generates a simple subset of images to test downloading, setup first


* **Returns**

    None



### rgps_preprocessing.examples.example_get_all_matches()
Example for get all matches for a short time period


* **Returns**

    None



### rgps_preprocessing.examples.example_get_dataframes()

### rgps_preprocessing.examples.example_save_dataframes()

### rgps_preprocessing.examples.example_setup()
Runs setup over January 2019


* **Returns**

    None


## rgps_preprocessing.functions module


### rgps_preprocessing.functions.clean()
Clean all downloaded data
WARNING: Don’t do unless data is corrupted, you will lose hours of work


* **Returns**

    None



### rgps_preprocessing.functions.setup_config(working_directory='')
Setup the configuration for matching and directories
TODO: Add more configuration needed for NISAR and other platform compatibilities


* **Parameters**

    **working_directory** – Set working directory



* **Returns**

    None



### rgps_preprocessing.functions.setup_hem_daterange(start_date: datetime.date, end_date: datetime.date, platform: str, hemisphere: str)
Create and load all data directories for the given date range and save them to the file system
in yearly, monthly, daily nested directories


* **Parameters**

    
    * **start_date** – starting day (inclusive) of data to save


    * **end_date** – ending day (exclusive) of data to save


    * **platform** – satellite platform for data


    * **hemisphere** – hemisphere of data



* **Returns**

    None


## rgps_preprocessing.geocoding module


### _class_ rgps_preprocessing.geocoding.GdalRaster(filepath, \*\*kwargs)
Bases: `object`

GDAL Raster class. Opens GeoTIFF using GDAL, geocodes, and extracts useful
metadata. Includes methods for:


#### close()

#### get_extent()

#### raw_to_array()

#### reproject(pixsp)
Reproject input GeoTIFF in memory from WSG86 to SSMI


### rgps_preprocessing.geocoding.crs_transform(src_polygon: <module 'shapely.geometry.polygon' from '/Users/nathanc/Documents/Code/env/lib/python3.9/site-packages/shapely/geometry/polygon.py'>, src_crs: str, tgt_crs: str)
Transforms a Well Known Text shape from source coordinate reference system to target coordinate reference system
implementation of [https://gis.stackexchange.com/questions/127427/transforming-shapely-polygon-and-multipolygon-objects](https://gis.stackexchange.com/questions/127427/transforming-shapely-polygon-and-multipolygon-objects)


* **Parameters**

    
    * **src_polygon** – source shapely polygon


    * **src_crs** – Source coordinate reference system


    * **tgt_crs** – Target coordinate reference system



* **Returns**

    well known text string transformed to target coordinate system



### rgps_preprocessing.geocoding.get_hemisphere(bounding_box: Tuple[int, int, int, int])
TODO: find a better way to figure out hemispheres
Get the hemisphere of the given bounding box


* **Parameters**

    **bounding_box** – tuple of the form (ul_lat, ul_lon, lr_lat, lr_lon)



* **Returns**

    hemisphere containing the bounding box



### rgps_preprocessing.geocoding.lonlat2ssmi(lat, lon, hem)
Convert longitude and latitude (WGS84) to SSMI coordinates
NOTE: not sure if this belongs here either


* **Parameters**

    
    * **lat** – latitude (WGS84)


    * **lon** – longitude (WGS84)


    * **hem** – hemisphere- NORTH or ‘south’



* **Returns**

    x and y coordinates in SSMI projection



### rgps_preprocessing.geocoding.warp_dataset(amsr_path: pathlib.Path, warp_options: dict)
Call gdal.Warp with specified options on input AMSR file.
NOTE: not sure if this belongs here, I’ll leave it up to your discretion


* **Parameters**

    
    * **amsr_path** – Path to daily AMSR file


    * **warp_options** – A dictionary containing options for gdal.Warp



* **Returns**

    A 2d numpy.ndarray with ice concentration values


## rgps_preprocessing.grid_setup module


### rgps_preprocessing.grid_setup.find_nxny(xbuf, ybuf)
Given grid point 1D of map location arrays, find the grid spacing and
2D dimensions of the data. Assumes grid spacing is the same in x and y 
directions.


* **Parameters**

    
    * **xbuf** – X buffer


    * **ybuf** – Y buffer



* **Returns**

    grid spacing (gridsp) and 2D dimensions (ngridx and ngridy)



### rgps_preprocessing.grid_setup.generate_fillgrid(outpath, nimage, nsrcgrid, styd, stimgid, sgxx, sgyy, stxul, styul, stpixsp, stnpix, stnrec, src_gridid)
Generate .fillgrid file


* **Parameters**

    
    * **outpath** – Path to output .fillgrid file


    * **nimage** – Number of images (1 source + X target images)


    * **nsrcgrid** – Number of grid points of source image


    * **styd** – list of dates for source and target images expressed as 
    YYYYDOY, e.g. 2019097 for April 7?, 2019
    [src_yd, trg_yd1, …, trg_ydX] for target images 1, …, X


    * **stimgid** – list of image IDs [src_id, trg_id1, …, trg_idX]


    * **sgxx** – list of grid X coordinates in kilometers


    * **sgyy** – list of grid Y coordinates in kilometers


    * **stxul** – list of upper left x coordinates in projected units
    [src_ulx, trg_ulx1, …, trg_ulxX]


    * **styul** – list of upper left y coordinates in projected units
    [src_uly, trg_uly1, …, trg_ulyX]


    * **stpixsp** – list of pixel spacing for images
    [src_pixsp, trg_pixsp1, …, trg_pixspX]


    * **stnpix** – list of number of pixels for images 
    [src_npix, trg_npix1, …, trg_npixX]


    * **stnrec** – list of number of records for images
    [src_nrec, trg_nrec1, …, trg_nrecX]


    * **src_gridid** – list of source grid point IDs 
    e.g. [1010, 1011, …, 4444]



* **Returns**

    None; write .fillgrid file to disk



### rgps_preprocessing.grid_setup.genereate_fillconn(outpath, ncell, cellgrid, styd)
Generate connectivity table (.fillconn file)


* **Parameters**

    
    * **outpath** – Path to output file (.fillconn)


    * **ncell** – Number of grid cells


    * **cellgrid** – Cell grid from setup_cellgrid


    * **styd** – List of datestrings formatted as integers: YYYYDOY
    [src_yd, trg_yd1, trg_yd2, …, trgydX]



* **Returns**

    None; write .fillconn file to disk



### rgps_preprocessing.grid_setup.setup_cellgrid(gid, year, doy, gnx, gny, nimage)
Set up cell grid for IMV processing


* **Parameters**

    
    * **gid** – 2d array of grid IDs (0= cell not included?; generated by find_nxny)


    * **year** – Year of source image


    * **doy** – Day of year of source image


    * **gnx** – X dimension of grid (from find_nxny)


    * **gny** – Y dimension of grid (from find_nxny)



* **Returns**

    the cell grid (2d numpy array) and number of cells (int)


## rgps_preprocessing.ice_concentration module


### rgps_preprocessing.ice_concentration.build_amsr_filename(day: datetime.date, hemisphere: str)
Build file name for AMSR ice concentration files for local filesystem
and http request


* **Parameters**

    
    * **day** – date of the ice concentration file


    * **hemisphere** – hemisphere of the ice concentration file



* **Returns**

    AMSR ice concentration filename



### rgps_preprocessing.ice_concentration.build_amsr_path(day: datetime.date, hemisphere: str)
Get path of AMSR ice concentration file for the given day and hemisphere,
creating it if it does not exist


* **Parameters**

    
    * **day** – date of the ice concentration file


    * **hemisphere** – hemisphere of the ice concentration file



* **Returns**

    ice concentration file path



### rgps_preprocessing.ice_concentration.build_amsr_url(day: datetime.date, hemisphere: str)
Get url for downloading AMSR ice concentration data from the University of Bremen’s file system


* **Parameters**

    
    * **day** – date of ice concentration file


    * **hemisphere** – hemisphere of ice concentration file



* **Returns**

    URL of ice concentration file



### rgps_preprocessing.ice_concentration.daily_download_amsr_data(day: datetime.date, hemisphere: str)
Download a single day’s AMSR ice concentration data from the University of Bremen’s file system


* **Parameters**

    
    * **day** – date of ice concentration data to be downloaded


    * **hemisphere** – hemisphere of ice concentration data to be downloaded



* **Returns**

    None



* **Raises**

    **IOError** – if file cannot be downloaded (the University’s website has been down a couple times so far)



### rgps_preprocessing.ice_concentration.daterange_amsr_download_data(start_date: datetime.date, end_date: datetime.date, hemisphere: str)
Download AMSR ice concentration data for the given date range (inclusive, exclusive) from the
University of Bremen’s file system


* **Parameters**

    
    * **start_date** – start date (inclusive) of ice concentration data to be downloaded


    * **end_date** – end date (exclusive) of ice concentration data to be downloaded


    * **hemisphere** – hemisphere of ice concentration data to be downloaded



* **Returns**

    None



* **Raises**

    **IOError** – if file cannot be downloaded (the University’s website has been down a couple times so far)



### rgps_preprocessing.ice_concentration.get_land_and_ice_percentage(bounding_box, amsr_path: pathlib.Path = PosixPath('.'))
Extract land and ice coverage as percent of total image area
NOTE: I’m assuming we’re only using AMSR data for now
Also, I’m assuming we can get both land and ice out of this, though
it might turn out that we can only use AMSR for ice due to resolution


* **Parameters**

    
    * **bounding_box** – bounding box of the form (ul_xmeters, ul_ymeters, lr_xmeters, lr_ymeters)


    * **amsr_path** – Path to AMSR file (should be uniquely identified by datestr)



* **Returns**

    land and ice coverage as percents


## rgps_preprocessing.matching module


### rgps_preprocessing.matching.get_all_matches(start_time: datetime.datetime, end_time: datetime.datetime, platform: str, bounding_box: dict)
Get all possible matches in a given bounding box for a given time range


* **Parameters**

    
    * **start_time** – beginning of time range


    * **end_time** – end of time range


    * **platform** – satellite platform of the matching data


    * **bounding_box** – bounding box of interest for matching



* **Returns**

    


### rgps_preprocessing.matching.get_target_df_matches(target: pandas.core.series.Series, gdf: geopandas.geodataframe.GeoDataFrame)
Get a GeoDataFrame of all potential source images that match the target image
Note: RGPS preprocessing was done in such a way that it matches the target image to possible sources


* **Parameters**

    
    * **target** – target image to match against


    * **gdf** – GeoDataFrame of interest containing possible source images (GeoDataFrame from around the same time and in the same region)



* **Returns**

    GeoDataFrame of potential source images that meet the thresholds of matching for target image


## rgps_preprocessing.util module


### rgps_preprocessing.util.build_daily_path(base_path: pathlib.Path, day: datetime.date, hemisphere: str, file_name: str = '', daily_directory: bool = False)
Create a path to an file/directory from the base_path that is organized by hemisphere, year, and month,
creating any subdirectories if they don’t exist


* **Parameters**

    
    * **base_path** – base directory to build directory structure in


    * **day** – date of the file or directory


    * **hemisphere** – hemisphere of interest


    * **file_name** – name of file to return, otherwise leave empty to return just the directory path


    * **daily_directory** – create a directory for every day of the year if True else only create directories per month



* **Returns**

    path to local AMSR ice data file



### rgps_preprocessing.util.daterange(start_date: datetime.date, end_date: datetime.date)
Creates a generator for a range of dates between the start date and end date (inclusive exclusive)


* **Parameters**

    
    * **start_date** – start date of the date range (included)


    * **end_date** – end date of the date range (excluded)



* **Returns**

    date range generator object



### rgps_preprocessing.util.verify_dir(directory: pathlib.Path)
Returns directory path, creating the required directories if they don’t exist


* **Parameters**

    **directory** – directory to verify existence of



* **Returns**

    verified directory path


## Module contents
